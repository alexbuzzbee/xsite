# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
Main XSite script.

Syntax: xsite.py <site>

Generates the XSite site in the directory hierarchy rooted at <site>. Reads configuration from <site>/config.ini, templates and processor files (e.g. XSLT stylesheets) from <site>/templates/, documents from <site>/documents/, and resources from <site>/resources/published/ and <site>/resources/unpublished/. Deletes <site>/output/, then recreates it and and writes the output there.
"""
import subprocess
import sys
import configparser
import shutil
from pathlib import Path

serial = False
if sys.argv[1] == "--serial":
    serial = True
    sys.argv[1] = sys.argv[2]

site = Path(sys.argv[1])

config = configparser.ConfigParser()
config.read(site / "config.ini")

def config_args(path):
    """
    Determine the configuration that applies to a particular path and serialize it for passing on the command line.
    """
    relative = path.relative_to(site / "documents")
    kvs = {}
    
    kvs.update(config["DEFAULT"]) # Configuration sections specify paths to which particular values apply. More specific sections override less specific ones.
    for prefix in list(relative.parents)[::-1][1:]: # [::-1][1:] iterates from second-to-last in reverse order.
        try:
            kvs.update(config[str(prefix)])
        except KeyError: pass # No config section for this prefix, that's fine.
    try:
        kvs.update(config[str(relative)])
    except KeyError: pass # No config section for this file, that's fine.
    kvs["templates"] = site / "templates"
    kvs["resources"] = site / "resources"
    kvs["documents"] = site / "documents"
    
    args = []
    for key, value in kvs.items():
        args += [key, value]
    return args

try:
    shutil.rmtree(site / "output")
except FileNotFoundError: pass # Already not there, that's fine.

shutil.copytree(site / "resources" / "published", site / "output")

# Process all documents.
processes = []
for name in (site / "documents").glob("**/*.xml"):
    (site / "output" / name.relative_to(site / "documents")).parents[0].mkdir(parents=True, exist_ok=True)
    process = subprocess.Popen(["python3", Path(sys.path[0]) / "xsite_process.py", name] + config_args(name), stdout=(site / "output" / name.relative_to(site / "documents")).open("wb"))
    if serial:
        process.wait()
    else:
        processes.append(process)

if not serial:
    for process in processes:
        process.wait()
