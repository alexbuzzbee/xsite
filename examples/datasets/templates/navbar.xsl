<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="dataset">
    <nav>
      <ul>
        <xsl:apply-templates />
      </ul>
    </nav>
  </xsl:template>
  <xsl:template match="record">
    <li><a><xsl:attribute name="href"><xsl:value-of select="@path" /></xsl:attribute><xsl:value-of select="@name" /></a></li>
  </xsl:template>
</xsl:stylesheet>
